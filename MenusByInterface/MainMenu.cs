﻿namespace MenusByInterface
{
    public class MainMenu
    {
        private MenuItem MenuItem = null;

        public MainMenu(MenuItem parentMenuItem)
        {
            this.MenuItem = parentMenuItem;
        }

        public void show()
        {
            this.MenuItem.HandleMenuItem();
        }
    }
}