﻿using System;
using System.Collections.Generic;
using System.Threading;

namespace MenusByInterface
{
    public class MenuItem
    {
        private string title;
        private List<MenuItem> MenuItemsList = new List<MenuItem>();
        private IAction action = null;
        private int actionDuration = 4000;
        private MenuItem parentMenuItem = null;
        

        public MenuItem(string title, IAction action)
        {
            this.title = title;
            this.action = action;
            this.parentMenuItem = parentMenuItem;
            //MenuItemsList.Add(parentMenuItem==null ? new MenuItem("Exit",null) : new MenuItem("Back",null) );
        }

        public void HandleMenuItem()
        {
            if (this.action == null) handleMenuItemShow();
            else preformAction();
        }
        
        private void handleMenuItemShow()
        {
            bool isBackOrExitRequested = false;       
            while (!isBackOrExitRequested)
            {
                showMenu();
                int userChoice = MenusByInterface.userChoice.GetValidIntWithRange(
                    0, MenuItemsList.Count);
                if (userChoice == 0)
                {
                    isBackOrExitRequested = true;
                    if(parentMenuItem == null)Environment.Exit(0);
                }
                else MenuItemsList[userChoice - 1].HandleMenuItem();
            }
        }

        private void showMenu()
        {
            Console.Clear();
            Console.WriteLine(this.title + ":");
            int i = 0;
            foreach (MenuItem menuItem in MenuItemsList)
            {
                Console.WriteLine("{0}.{1}", ++i,menuItem.title);
            }

            Console.WriteLine(parentMenuItem == null ? "0.Exit" : "0.Back");
        }

        private void preformAction()
        {
            Console.Clear();
            action.Execute();
            Thread.Sleep(actionDuration);
        }

        public void addMenuItem(MenuItem newMenuItem)
        {
            newMenuItem.parentMenuItem = this;
            this.MenuItemsList.Add(newMenuItem);
        }
    }
}