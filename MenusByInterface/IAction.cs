﻿namespace MenusByInterface
{
    public interface IAction
    {
        void Execute();
    }
}