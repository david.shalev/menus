﻿using System;

namespace MenusByInterface
{
    public  class userChoice
    {
        public static int GetValidIntWithRange(int min, int max)
        {
            while (true)
            {
                Console.WriteLine("please select number");
                bool isNumber = Int32.TryParse(Console.ReadLine(), out int selectedNumber);
                if (!isNumber)
                {
                    Console.WriteLine("the given input is not a number");
                    continue;
                }

                if (selectedNumber < min || selectedNumber > max)
                {
                    Console.WriteLine("the given number is not in the range");
                    continue;
                }

                return selectedNumber;
            }
        }
    }
}