﻿using System;
using MenusByInterface;

namespace MenusTest.Actions
{
    public class ShowTime : IAction
    {
        public void Execute()
        {
            Console.WriteLine("now is {0}", DateTime.Now.ToString("hh:mm:ss tt"));
        }
    }
}