﻿using System;
using MenusByInterface;

namespace MenusTest.Actions
{
    public class ShowVersion :IAction
    {
        public void Execute()
        {
            Console.WriteLine("Version: 19.2.4.32");
        }
    }
}