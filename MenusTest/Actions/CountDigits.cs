﻿using System;
using MenusByInterface;

namespace MenusTest.Actions
{
    public class CountDigits : IAction
    {
        public void Execute()
        {
            Console.WriteLine("please type something");
            string userText = Console.ReadLine();
            int sumOfDigits = 0;
            for (int i = 0; i < userText.Length; i++)
            {
                if (char.IsDigit(userText[i])) sumOfDigits++;
            }
            Console.WriteLine("Number of digits: {0}",sumOfDigits);
        }       
    }
}