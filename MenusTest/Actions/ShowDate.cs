﻿using System;
using System.Runtime.Remoting.Channels;
using MenusByInterface;

namespace MenusTest.Actions
{
    public class ShowDate :IAction
    {
        public void Execute()
        {
            Console.WriteLine("Today is {0}",DateTime.Now.ToShortDateString());
        }
    }
}