﻿using System;
using MenusByInterface;
using MenusTest.Actions;

namespace MenusTest
{
    internal class InterfaceMenu
    {
        public static void handleInterfaceMenu()
        {
            MenuItem mainMenuItem = new MenuItem("Main Menu",null);
            MenuItem showDateTime = new MenuItem("Show date/time",null);
            MenuItem versionAndDigits =new MenuItem("version and digits",null);
            MenuItem showDate =new MenuItem("Show Date",new ShowDate());
            MenuItem showTime = new MenuItem("Show Time",new ShowTime());
            MenuItem countDigits =new MenuItem("Count digits",new CountDigits());
            MenuItem showVersion =new MenuItem("Show version",new ShowVersion());
            showDateTime.addMenuItem(showDate);
            showDateTime.addMenuItem(showTime);
            versionAndDigits.addMenuItem(countDigits);
            versionAndDigits.addMenuItem(showVersion);
            mainMenuItem.addMenuItem(showDateTime);
            mainMenuItem.addMenuItem(versionAndDigits);
            MainMenu mainMenu =new MainMenu(mainMenuItem);
            mainMenu.show();
        }


    }
}